package Proyecto;

import java.io.*;
import java.net.Socket;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.DefaultListModel;


public class ChatClient extends Thread{
    //class OyenteCliente implements ActionListener{
    private Socket SocketCliente;
    private DataInputStream entrada;
    private PanelChat cliente;
    private ObjectInputStream entradaObjeto;
    
    public ChatClient(Socket SocketCliente, PanelChat cliente) { //Se crea en todo este proceso el ingreso de los clientes
        this.SocketCliente = SocketCliente;
        this.cliente = cliente;
        
    }
    
    public void run() {
        while(true) {
            try {
                entrada =  new DataInputStream(SocketCliente.getInputStream());
                cliente.mensajeria(entrada.readUTF());
                
                entradaObjeto = new ObjectInputStream(SocketCliente.getInputStream());
                cliente.actualizarLista((DefaultListModel) entradaObjeto.readObject());
            } catch (ClassNotFoundException ex) {
                Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);//Se obtiene los datos para hacer "login"
            } catch (IOException ex) {
                Logger.getLogger(ChatClient.class.getName()).log(Level.SEVERE, null, ex);
            }
        }
    }
          
    
    
}
 